/****************************************************************************
 
  Header file for BrushlessMotorTestSM State Machine 
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef BrushlessMotorTestSM_H
#define BrushlessMotorTestSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitMotorTest, Off, On } BrushlessMotorState_t ;


// Public Function Prototypes

bool InitBrushlessMotorTestSM ( uint8_t Priority );
bool PostBrushlessMotorTestSM( ES_Event ThisEvent );
ES_Event RunBrushlessMotorTestSM( ES_Event ThisEvent );
BrushlessMotorState_t QueryBrushlessMotorTestSM ( void );
uint8_t getInc(void);


#endif /* BrushlessMotorTestSM_H */

