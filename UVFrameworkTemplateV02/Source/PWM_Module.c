/****************************************************************************
 Module
   PWM_Module.c

 Revision
   1.0.1

 Description
   This module will initialize and control 4 PWM outputs on the tiva.
	 The outputs used will use module 0 - generator 1&2. This corresponds
	 to pins PB6 (PWM0-0-0) PB7 (PWM0-0-1) PB4 (PWM0-1-0) and PB5 (PWM0-1-1).

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec     began conversion from TemplateFSM.c
 01/25/17	08:10	jat			create PWM Module
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/pwm.h"
#include "termio.h"

#include "BITDEFS.H"

//My Includes
#include "PWM_Module.h"
#include "Hardware.h"


/*----------------------------- Module Defines ----------------------------*/
// Number of bits in 1 nibble
//#define ALL_BITS (0xFF << 2)
#define BITS_IN_NIBBLE 4
#define MAX_16_BIT 0xFFFF

/*
// Pin defines
#define MOTOR_PORT GPIO_PORTB_BASE
#define MOTOR_1A GPIO_PIN_6 //port B
#define MOTOR_1B GPIO_PIN_7 //port B
#define MOTOR_0A GPIO_PIN_4 //port B
#define MOTOR_0B GPIO_PIN_5 //port B
*/

// Alternate pin function defines
#define AFSEL_MUX_PB4_M 0x000F0000
#define AFSEL_PWM2_PB4 (4 << (4*BITS_IN_NIBBLE))

#define AFSEL_MUX_PB5_M 0x00F00000
#define AFSEL_PWM3_PB5 (4 << (5*BITS_IN_NIBBLE))

#define AFSEL_MUX_PB6_M 0x0F000000
#define AFSEL_PWM0_PB6 (4 << (6*BITS_IN_NIBBLE))

#define AFSEL_MUX_PB7_M 0xF0000000
#define AFSEL_PWM1_PB7 (4 << (7*BITS_IN_NIBBLE))

#define AFSEL_MUX_PC4_M 0x000F0000
#define AFSEL_PWM6_PC4 (4 << (4*BITS_IN_NIBBLE))

#define AFSEL_MUX_PC5_M 0x00F00000
#define AFSEL_PWM7_PC5 (4 << (5*BITS_IN_NIBBLE))

#define AFSEL_MUX_PD0_M 0x0000000F
#define AFSEL_PWM0_PD0 (5 << (0*BITS_IN_NIBBLE))

#define AFSEL_MUX_PD1_M 0x000000F0
#define AFSEL_PWM1_PD1 (5 << (1*BITS_IN_NIBBLE))

#define AFSEL_MUX_PE4_M 0x000F0000
#define AFSEL_PWM2_PE4 (5 << (4*BITS_IN_NIBBLE))

#define AFSEL_MUX_PE5_M 0x00F00000
#define AFSEL_PWM3_PE5 (5 << (5*BITS_IN_NIBBLE))

// Clock defines
#define SYSTEM_CLOCK_FREQ 40000000
#define PWM_CLOCK_DIV SYSCTL_RCC_PWMDIV_16
#define DEFAULT_FREQ 1000
#define DEFAULT_DUTY_PER 0

// PWM Generator defines
#define PWM0_GENERATOR_0 0
#define PWM0_GENERATOR_1 1
#define PWM0_GENERATOR_3 2
#define PWM1_GENERATOR_0 3
#define PWM1_GENERATOR_1 4

#define PWM_NORMAL_POL 0
#define PWM_INV_POL 1
#define CHANNEL_A 0
#define CHANNEL_B 1

// Debugging messages
#define DEBUG_DUTY 0

// Test harness
//#define TEST

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
// Function to configure GPIO hardware for PWM
void PWM_GPIO_HardwareInit( void );

// Function to deal with 0% duty cycle
void PWM_SetDuty0( uint8_t generator, uint8_t channel );

// Function to deal with 100% duty cycle
void PWM_SetDuty100( uint8_t generator, uint8_t channel );

// Restore normal up/down functionality to generator
void PWM_UpDownNormal( uint8_t generator, uint8_t channel );

void PWM_SetDutyPeriodTicks( int32_t newPeriodTicks, uint8_t generator, uint8_t channel );

/*---------------------------- Module Variables ---------------------------*/
// PWM clock divisor
#if PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_1
	static const uint8_t clockDivisor = 1;
#elif PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_2
	static const uint8_t clockDivisor = 2;
#elif PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_4
	static const uint8_t clockDivisor = 4;
#elif PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_8
	static const uint8_t clockDivisor = 8;
#elif PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_16
	static const uint8_t clockDivisor = 16;
#elif PWM_CLOCK_DIV == SYSCTL_RCC_PWMDIV_32
	static const uint8_t clockDivisor = 32;
#else
	static const uint8_t clockDivisor = 64;
#endif

// Determine clock ticks per ms
static float PWM_TicksPer_uS = TicksPerUS_f / clockDivisor;

// Desired PWM period in clock ticks
static uint16_t PWMPeriod0_0;
static uint16_t PWMPeriod0_1;
static uint16_t PWMPeriod0_3;
static uint16_t PWMPeriod1_0;
static uint16_t PWMPeriod1_1;

// PWM Inversion flags
static bool Inverted0_0=false;
static bool Inverted0_1=false;
static bool Inverted0_3=false;
static bool Inverted1_0=false;
static bool Inverted1_1=false;

// Flag for PWM hardware already initialized
static bool PWM_Initialized = false;


/*----------------------------- Public Functions --------------------------*/
/****************************************************************************
 Function
     InitPWMHardware

 Parameters
     numGenerators (0 initializes PWM0 generator0 only, 1 initializes
				PWM0 generator1 in addition to generator0)

 Returns
     nothing

 Description
     Initializes and configures hardware for PWM use
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_InitHardware( void ){
	if ( PWM_Initialized == false ){
		// Enable clock to the PWM Module
		HWREG( SYSCTL_RCGCPWM ) |= SYSCTL_RCGCPWM_R0;
		HWREG( SYSCTL_RCGCPWM ) |= SYSCTL_RCGCPWM_R1;
		
		// Check if clock is enabled to port B - if not, enable it
		if ( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1 ){
			// Enable port B clock
			HWREG( SYSCTL_RCGCGPIO ) |= SYSCTL_RCGCGPIO_R1;
			
			// Loop until port is ready
			while( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1 );
		}
		
		// Check if clock is enabled to port C - if not, enable it
		if ( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2 ){
			// Enable port C clock
			HWREG( SYSCTL_RCGCGPIO ) |= SYSCTL_RCGCGPIO_R2;
			
			// Loop until port is ready
			while( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2 );
		}
		
		// Check if clock is enabled to port D - if not, enable it
		if ( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R3) != SYSCTL_PRGPIO_R3 ){
			// Enable port D clock
			HWREG( SYSCTL_RCGCGPIO ) |= SYSCTL_RCGCGPIO_R3;
			
			// Loop until port is ready
			while( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R3) != SYSCTL_PRGPIO_R3 );
		}
		
		// Check if clock is enabled to port E - if not, enable it
		if ( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4 ){
			// Enable port E clock
			HWREG( SYSCTL_RCGCGPIO ) |= SYSCTL_RCGCGPIO_R4;
			
			// Loop until port is ready
			while( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4 );
		}
		
		// Select 32 divisor for system clock into PWM module
		HWREG( SYSCTL_RCC ) = ( HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M ) |
			( SYSCTL_RCC_USEPWMDIV | PWM_CLOCK_DIV );
		
		// Loop until PWM hardware is ready
		while( (HWREG( SYSCTL_PRPWM ) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0 );
		while( (HWREG( SYSCTL_PRPWM ) & SYSCTL_PRPWM_R1) != SYSCTL_PRPWM_R1 );
		
		// Disable PWM hardware while configuring system
		HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0;
		HWREG( PWM0_BASE+PWM_O_1_CTL ) = 0;
		HWREG( PWM0_BASE+PWM_O_3_CTL ) = 0;
		HWREG( PWM1_BASE+PWM_O_0_CTL ) = 0;
		HWREG( PWM1_BASE+PWM_O_1_CTL ) = 0;
		
		// Program generators for up/down timing. Pins will go hi at rising
		// compare A/B, will go lo at falling compare A/B
		// Generator A
		PWM_UpDownNormal( PWM0_GENERATOR_0, CHANNEL_A );
		PWM_UpDownNormal( PWM0_GENERATOR_0, CHANNEL_B );
		
		PWM_UpDownNormal( PWM0_GENERATOR_1, CHANNEL_A );
		PWM_UpDownNormal( PWM0_GENERATOR_1, CHANNEL_B );
		
		PWM_UpDownNormal( PWM0_GENERATOR_3, CHANNEL_A );
		PWM_UpDownNormal( PWM0_GENERATOR_3, CHANNEL_B );
		
		PWM_UpDownNormal( PWM1_GENERATOR_0, CHANNEL_A );
		PWM_UpDownNormal( PWM1_GENERATOR_0, CHANNEL_B );
		
		PWM_UpDownNormal( PWM1_GENERATOR_1, CHANNEL_A );
		//PWM_UpDownNormal( PWM1_GENERATOR_1, CHANNEL_B );
		
		// Set initial frequency at 1000Hz as a default
		PWM_SetFrequency( DEFAULT_FREQ, PWM0_GENERATOR_0 );
		PWM_SetFrequency( DEFAULT_FREQ, PWM0_GENERATOR_1 );
		PWM_SetFrequency( DEFAULT_FREQ, PWM0_GENERATOR_3 );
		PWM_SetFrequency( DEFAULT_FREQ, PWM1_GENERATOR_0 );
		PWM_SetFrequency( DEFAULT_FREQ, PWM1_GENERATOR_1 );
		
		// Set initial duty cycle to default
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_0, CHANNEL_A );
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_0, CHANNEL_B );
		
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_1, CHANNEL_A );
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_1, CHANNEL_B );
		
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_3, CHANNEL_A );
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM0_GENERATOR_3, CHANNEL_B );
		
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM1_GENERATOR_0, CHANNEL_A );
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM1_GENERATOR_0, CHANNEL_B );
		
		PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM1_GENERATOR_1, CHANNEL_A );
		//PWM_SetDutyCycle( DEFAULT_DUTY_PER, PWM1_GENERATOR_1, CHANNEL_B );
		
		// Enable PWM	outputs
		HWREG( PWM0_BASE+PWM_O_ENABLE ) |= ( PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN
			| PWM_ENABLE_PWM7EN );
			
		HWREG( PWM1_BASE+PWM_O_ENABLE ) |= ( PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM2EN );
		
		// Configure GPIO hardware
		PWM_GPIO_HardwareInit();
		
		// Enable PWM generators, set counting mode to up/down, and
		// set generator updates to be locally synchronized
		HWREG( PWM0_BASE+PWM_O_0_CTL ) = ( PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
			PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS );
			
		HWREG( PWM0_BASE+PWM_O_1_CTL ) = ( PWM_1_CTL_MODE | PWM_1_CTL_ENABLE |
			PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS );
			
		HWREG( PWM0_BASE+PWM_O_3_CTL ) = ( PWM_3_CTL_MODE | PWM_3_CTL_ENABLE |
			PWM_3_CTL_GENAUPD_LS | PWM_3_CTL_GENBUPD_LS );
			
		HWREG( PWM1_BASE+PWM_O_0_CTL ) = ( PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
			PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS );
			
		HWREG( PWM1_BASE+PWM_O_1_CTL ) = ( PWM_1_CTL_MODE | PWM_1_CTL_ENABLE |
			PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS );
			
		// Set PWM initialized flag
		PWM_Initialized = true;
	}
}

/****************************************************************************
 Function
     PWM_SetFrequency

 Parameters
     uint16_t newFrequency

 Returns
     nothing

 Description
     Updates PWM generator frequency
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetFrequency( uint16_t newFrequency, uint8_t generatorNum ){
	// local new period variable
	uint16_t newPeriod;
	
	// Ensure new frequency is within hardware config limits
	if ( newFrequency > (SysCtlClockGet() / clockDivisor) ){
		newFrequency = SysCtlClockGet() / clockDivisor;
		
		// Calculate new period
		newPeriod = (SysCtlClockGet() / clockDivisor) / newFrequency;
	}
	else if ( ((SysCtlClockGet() / clockDivisor) / newFrequency) > MAX_16_BIT ){
		newPeriod = MAX_16_BIT;
	}
	else{	
		// Calculate new period
		newPeriod = (SysCtlClockGet() / clockDivisor) / newFrequency;
	}
	
	// Determine appropriate generator
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Write 0.5*PWMPeriod into load register due to up/down mode
		HWREG( PWM0_BASE+PWM_O_0_LOAD ) = newPeriod >> 1;
		
		// Update generator period
		PWMPeriod0_0 = newPeriod;
		
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Write 0.5*PWMPeriod into load register due to up/down mode
		HWREG( PWM0_BASE+PWM_O_1_LOAD ) = newPeriod >> 1;
		
		// Update generator period
		PWMPeriod0_1 = newPeriod;
		
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Write 0.5*PWMPeriod into load register due to up/down mode
		HWREG( PWM0_BASE+PWM_O_3_LOAD ) = newPeriod >> 1;
		
		// Update generator period
		PWMPeriod0_3 = newPeriod;
		
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Write 0.5*PWMPeriod into load register due to up/down mode
		HWREG( PWM1_BASE+PWM_O_0_LOAD ) = newPeriod >> 1;
		
		// Update generator period
		PWMPeriod1_0 = newPeriod;
		
	}else{
		// Write 0.5*PWMPeriod into load register due to up/down mode
		HWREG( PWM1_BASE+PWM_O_1_LOAD ) = newPeriod >> 1;
		
		// Update generator period
		PWMPeriod1_1 = newPeriod;
		
	}
	
}

/****************************************************************************
 Function
     PWM_SetDutyPeriod

 Parameters
     uint8_t newDuty, uint8_t channel

 Returns
     nothing

 Description
     Updates PWM channel duty cycle
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetDutyPeriod( float newPeriod_uS, uint8_t generatorNum, uint8_t channel ){
	float newPerVal;
	uint16_t currentPeriod;
	uint16_t newCompVal;
	
	// Get appropriate current period
	switch ( generatorNum ){
		case ( PWM0_GENERATOR_0 ):
			currentPeriod = PWMPeriod0_0;
			
			break;
		
		case ( PWM0_GENERATOR_1 ):
			currentPeriod = PWMPeriod0_1;
			
			break;
		
		case ( PWM0_GENERATOR_3 ):
			currentPeriod = PWMPeriod0_3;
			
			break;
		
		case ( PWM1_GENERATOR_0 ):
			currentPeriod = PWMPeriod1_0;
			
			break;
		
		case ( PWM1_GENERATOR_1 ):
			currentPeriod = PWMPeriod1_1;
			
			break;		
	}
		
	// Calculate new period in ticks
	newPerVal = (newPeriod_uS * PWM_TicksPer_uS);
	
	if ( newPerVal >= currentPeriod ){
		PWM_SetDuty100( generatorNum, channel );
	} else if ( newPerVal < 1 ){
		PWM_SetDuty0( generatorNum, channel );
	} else {
		// Set normal functionality
		PWM_UpDownNormal( generatorNum, channel );
	
		// Calculate correct comparator value
		newCompVal = (currentPeriod >> 1) - (newPerVal /2.0f );

		// Call set duty ticks function
		PWM_SetDutyPeriodTicks( newCompVal, generatorNum, channel );
	}
}

/****************************************************************************
 Function
     PWM_SetDutyCycle

 Parameters
     uint8_t newDuty, uint8_t channel

 Returns
     nothing

 Description
     Updates PWM channel duty cycle
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetDutyCycle( float newDuty, uint8_t generatorNum, uint8_t channel ){
	uint16_t currentPeriod;
	uint16_t newCompVal;
	
	if ( newDuty > 99.0f ){
		PWM_SetDuty100( generatorNum, channel );
	} else if ( newDuty < 1.0f ){
		PWM_SetDuty0( generatorNum, channel );
	} else {
		// Get appropriate current period
		switch ( generatorNum ){
			case ( PWM0_GENERATOR_0 ):
				currentPeriod = PWMPeriod0_0;
				
				break;
			
			case ( PWM0_GENERATOR_1 ):
				currentPeriod = PWMPeriod0_1;
				
				break;
			
			case ( PWM0_GENERATOR_3 ):
				currentPeriod = PWMPeriod0_3;
				
				break;
			
			case ( PWM1_GENERATOR_0 ):
				currentPeriod = PWMPeriod1_0;
				
				break;
			
			case ( PWM1_GENERATOR_1 ):
				currentPeriod = PWMPeriod1_1;
				
				break;		
		}
		
		// Set normal functionality
		PWM_UpDownNormal( generatorNum, channel );
			
		// Calculate correct comparator value
		newCompVal = ((currentPeriod >> 1) - ((newDuty*(currentPeriod>>1)) / 100));
		if (DEBUG_DUTY) printf("New comparator value = %" PRIu16 "\r\n", newCompVal);

		// Call set duty ticks function
		PWM_SetDutyPeriodTicks( (int16_t) newCompVal, generatorNum, channel );
	}
}

/****************************************************************************
 Function
     PWM_SetDutyPeriodTicks

 Parameters
     uint8_t newDuty, uint8_t channel

 Returns
     nothing

 Description
     Updates PWM channel duty cycle
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetDutyPeriodTicks( int32_t newPeriodTicks, uint8_t generatorNum, uint8_t channel ){
	// Initialize local variables
	uint32_t regOffset;
	
	// Select appropriate variables and offsets for correct generator
	switch ( generatorNum ){
		case ( PWM0_GENERATOR_0 ):
			regOffset = PWM0_BASE;
		
			if ( channel == CHANNEL_A ){
				regOffset = regOffset + PWM_O_0_CMPA;
			}else{
				regOffset = regOffset + PWM_O_0_CMPB;
			}
			
			break;
		
		case ( PWM0_GENERATOR_1 ):
			regOffset = PWM0_BASE;
			
			if ( channel == CHANNEL_A ){
				regOffset = regOffset + PWM_O_1_CMPA;
			}else{
				regOffset = regOffset + PWM_O_1_CMPB;
			}
		
			break;
		
		case ( PWM0_GENERATOR_3 ):
			regOffset = PWM0_BASE;
			
			if ( channel == CHANNEL_A ){
				regOffset = regOffset + PWM_O_3_CMPA;
			}else{
				regOffset = regOffset + PWM_O_3_CMPB;
			}
		
			break;
		
		case ( PWM1_GENERATOR_0 ):
			regOffset = PWM1_BASE;
		
			if ( channel == CHANNEL_A ){
				regOffset = regOffset + PWM_O_0_CMPA;
			}else{
				regOffset = regOffset + PWM_O_0_CMPB;
			}
		
			break;
		
		case ( PWM1_GENERATOR_1 ):
			regOffset = PWM1_BASE;
			
			if ( channel == CHANNEL_A ){
				regOffset = regOffset + PWM_O_1_CMPA;
			}else{
				regOffset = regOffset + PWM_O_1_CMPB;
			}
		
			break;			
	}		
	
	// Load new comparator value
	HWREG( regOffset ) = newPeriodTicks;
}

/****************************************************************************
 Function
     PWM_SetPolarity

 Parameters
     uint8_t polarity (0 for normal, 1 for inverted)
		 uint8_t generatorNum (0 for generator0, 1 for generator1)
 Returns
     nothing

 Description
     Sets or resets inversion bit for PWM generator 0 or 1 - Channels 0 & 1
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetPolarity( uint8_t polarity, uint8_t generatorNum ){
	// Determine appropriate generator
			//if it is generator zero, then it is PWM 0 and 1 channels 
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Set appropriate polarity
		if ( polarity == PWM_NORMAL_POL ){
			HWREG( PWM0_BASE+PWM_O_INVERT ) &= ~(PWM_INVERT_PWM0INV | PWM_INVERT_PWM1INV);
			Inverted0_0 = false;
		}else{
			HWREG( PWM0_BASE+PWM_O_INVERT ) |= (PWM_INVERT_PWM0INV | PWM_INVERT_PWM1INV);
			Inverted0_0 = true;
		}
	
	//if it is generator one, then it is PWM 2 and 3 channels 
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Set appropriate polarity
		if ( polarity == PWM_NORMAL_POL ){
			HWREG( PWM0_BASE+PWM_O_INVERT ) &= ~(PWM_INVERT_PWM2INV | PWM_INVERT_PWM3INV);
			Inverted0_1 = false;
		}else{
			HWREG( PWM0_BASE+PWM_O_INVERT ) |= (PWM_INVERT_PWM2INV | PWM_INVERT_PWM3INV);
			Inverted0_1 = true;
		}
		
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Set appropriate polarity
		if ( polarity == PWM_NORMAL_POL ){
			HWREG( PWM0_BASE+PWM_O_INVERT ) &= ~(PWM_INVERT_PWM6INV | PWM_INVERT_PWM7INV);
			Inverted0_3 = false;
		}else{
			HWREG( PWM0_BASE+PWM_O_INVERT ) |= (PWM_INVERT_PWM6INV | PWM_INVERT_PWM7INV);
			Inverted0_3 = true;
		}
		
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Set appropriate polarity
		if ( polarity == PWM_NORMAL_POL ){
			HWREG( PWM1_BASE+PWM_O_INVERT ) &= ~(PWM_INVERT_PWM0INV | PWM_INVERT_PWM1INV);
			Inverted1_0 = false;
		}else{
			HWREG( PWM1_BASE+PWM_O_INVERT ) |= (PWM_INVERT_PWM0INV | PWM_INVERT_PWM1INV);
			Inverted1_0 = true;
		}
		
	}else{
		// Set appropriate polarity
		if ( polarity == PWM_NORMAL_POL ){
			HWREG( PWM1_BASE+PWM_O_INVERT ) &= ~(PWM_INVERT_PWM2INV | PWM_INVERT_PWM3INV);
			Inverted1_1 = false;
		}else{
			HWREG( PWM1_BASE+PWM_O_INVERT ) |= (PWM_INVERT_PWM2INV | PWM_INVERT_PWM3INV);
			Inverted1_1 = true;
		}
	}
}

/****************************************************************************
 Function
     PWM_GetPolarity

 Parameters
     uint8_t polarity (0 for normal, 1 for inverted)
		 uint8_t generatorNum (0 for generator0, 1 for generator1)
 Returns
     nothing

 Description
     Sets or resets inversion bit for PWM generator 0 or 1 - Channels 0 & 1
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
uint8_t PWM_GetPolarity( uint8_t generatorNum ){
	// Determine appropriate generator
			//if it is generator zero, then it is PWM 0 and 1 channels 
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Get appropriate polarity
		if ( Inverted0_0 == true ) return PWM_INV_POL;
		else return PWM_NORMAL_POL;
		
	//if it is generator one, then it is PWM 2 and 3 channels 
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Get appropriate polarity
		if ( Inverted0_1 == true ) return PWM_INV_POL;
		else return PWM_NORMAL_POL;
		
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Get appropriate polarity
		if ( Inverted0_3 == true ) return PWM_INV_POL;
		else return PWM_NORMAL_POL;
		
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Get appropriate polarity
		if ( Inverted1_0 == true ) return PWM_INV_POL;
		else return PWM_NORMAL_POL;
		
	}else{
		// Get appropriate polarity
		if ( Inverted1_1 == true ) return PWM_INV_POL;
		else return PWM_NORMAL_POL;
	}
}

/*---------------------------- Private Functions --------------------------*/
/****************************************************************************
 Function
     PWM_GPIO_HardwareInit

 Parameters
     numGenerators (0 initializes PWM0 generator0 only, 1 initializes
				PWM0 generator1 in addition to generator0)

 Returns
     nothing

 Description
     Initializes and configures GPIO hardware for PWM use
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_GPIO_HardwareInit( void ){
	// Configure pins PB6 and PB7 for PWM alternate function
	HWREG( MOTOR_PORT+GPIO_O_AFSEL ) |= (MOTOR_1A | MOTOR_1B);
	HWREG( MOTOR_PORT+GPIO_O_AFSEL ) |= (MOTOR_0A | MOTOR_0B);
	
	//pin PE4 for alternate function
	HWREG( LED_PORT+GPIO_O_AFSEL ) |= (STATUS_LED);

	HWREG( SHOOT_MOTOR_PORT+GPIO_O_AFSEL ) |= FLYWHEEL;
	HWREG( SHOOT_SERVO_PORT+GPIO_O_AFSEL ) |= LOAD_COW_SERVO;

	
	// Configure MUX to map PWM output to PB6 and PB7
	HWREG( MOTOR_PORT+GPIO_O_PCTL ) = (HWREG( MOTOR_PORT+GPIO_O_PCTL ) & 
		~AFSEL_MUX_PB6_M) + AFSEL_PWM0_PB6;
	HWREG( MOTOR_PORT+GPIO_O_PCTL ) = (HWREG( MOTOR_PORT+GPIO_O_PCTL ) & 
		~AFSEL_MUX_PB7_M) + AFSEL_PWM1_PB7;
	
	HWREG( MOTOR_PORT+GPIO_O_PCTL ) = (HWREG( MOTOR_PORT+GPIO_O_PCTL ) & 
			~AFSEL_MUX_PB4_M) + AFSEL_PWM2_PB4;
		HWREG( MOTOR_PORT+GPIO_O_PCTL ) = (HWREG( MOTOR_PORT+GPIO_O_PCTL ) & 
			~AFSEL_MUX_PB5_M) + AFSEL_PWM3_PB5;
	
	// Mux to PE4 for LED's
	HWREG( LED_PORT+GPIO_O_PCTL ) = (HWREG( LED_PORT+GPIO_O_PCTL ) & 
			~AFSEL_MUX_PE4_M) + AFSEL_PWM2_PE4;

	//MUX to map PWM output to PC5 and PD0 for shooting
	HWREG( SHOOT_MOTOR_PORT+GPIO_O_PCTL ) = (HWREG( SHOOT_MOTOR_PORT+GPIO_O_PCTL ) & 
		~AFSEL_MUX_PC5_M) + AFSEL_PWM7_PC5;
	
	HWREG( SHOOT_SERVO_PORT+GPIO_O_PCTL ) = (HWREG( SHOOT_SERVO_PORT+GPIO_O_PCTL ) & 
		~AFSEL_MUX_PD0_M) + AFSEL_PWM0_PD0;
	
	// Enable digital operations on pins PB6 and PB7, PC5 and PD0
	HWREG( MOTOR_PORT+GPIO_O_DEN ) |= (MOTOR_0A | MOTOR_0B);
	HWREG( MOTOR_PORT+GPIO_O_DEN ) |= (MOTOR_1A | MOTOR_1B);
	HWREG( SHOOT_MOTOR_PORT+GPIO_O_DEN ) |= FLYWHEEL;
	HWREG( SHOOT_SERVO_PORT+GPIO_O_DEN ) |= LOAD_COW_SERVO;
	HWREG( LED_PORT+GPIO_O_DEN ) |= (STATUS_LED);

	// Set direction of pins to output
	HWREG( MOTOR_PORT+GPIO_O_DIR ) |= (MOTOR_0A | MOTOR_0B);
	HWREG( MOTOR_PORT+GPIO_O_DIR ) |= (MOTOR_1A | MOTOR_1B);
	HWREG( SHOOT_MOTOR_PORT+GPIO_O_DIR ) |= FLYWHEEL;
	HWREG( SHOOT_SERVO_PORT+GPIO_O_DIR ) |= LOAD_COW_SERVO;
	HWREG( LED_PORT+GPIO_O_DIR ) |= (STATUS_LED);
}

/****************************************************************************
 Function
     PWM_SetDuty0

 Parameters
     generatorNum 	(0 for generator0 or 1 for generator1 in PWM0)
		 channel 		(0 for channel A or 1 for channel B)

 Returns
     nothing

 Description
     Update pwm channel to always off
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetDuty0( uint8_t generatorNum, uint8_t channel ){
	uint32_t regOffset;
	uint32_t regVal;
	
	// Determine appropriate generator
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_0_GENA;
				regVal = PWM_0_GENA_ACTZERO_ZERO;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_0_GENB;
			regVal = PWM_0_GENB_ACTZERO_ZERO;
		}
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_1_GENA;
				regVal = PWM_1_GENA_ACTZERO_ZERO;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_1_GENB;
			regVal = PWM_1_GENB_ACTZERO_ZERO;
		}
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_3_GENA;
				regVal = PWM_3_GENA_ACTZERO_ZERO;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_3_GENB;
			regVal = PWM_3_GENB_ACTZERO_ZERO;
		}
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_0_GENA;
				regVal = PWM_0_GENA_ACTZERO_ZERO;
		}
		else{
			regOffset = PWM1_BASE + PWM_O_0_GENB;
			regVal = PWM_0_GENB_ACTZERO_ZERO;
		}
	}else{
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_1_GENA;
				regVal = PWM_1_GENA_ACTZERO_ZERO;
		}
		else{
			regOffset = PWM1_BASE + PWM_O_1_GENB;
			regVal = PWM_1_GENB_ACTZERO_ZERO;
		}
	}
	
	// Program zero action to zero
	HWREG( regOffset ) = regVal;
}

/****************************************************************************
 Function
     PWM_SetDuty100

 Parameters
     generatorNum 	(0 for generator0 or 1 for generator1 in PWM0)
		 channel 		(0 for channel A or 1 for channel B)

 Returns
     nothing

 Description
     Update pwm channel to always on
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_SetDuty100( uint8_t generatorNum, uint8_t channel ){
	uint32_t regOffset;
	uint32_t regVal;
	
	// Determine appropriate generator
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_0_GENA;
				regVal = PWM_0_GENA_ACTZERO_ONE;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_0_GENB;
			regVal = PWM_0_GENB_ACTZERO_ONE;
		}
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_1_GENA;
				regVal = PWM_1_GENA_ACTZERO_ONE;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_1_GENB;
			regVal = PWM_1_GENB_ACTZERO_ONE;
		}
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_3_GENA;
				regVal = PWM_3_GENA_ACTZERO_ONE;
		}
		else{
			regOffset = PWM0_BASE + PWM_O_3_GENB;
			regVal = PWM_3_GENB_ACTZERO_ONE;
		}
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_0_GENA;
				regVal = PWM_0_GENA_ACTZERO_ONE;
		}
		else{
			regOffset = PWM1_BASE + PWM_O_0_GENB;
			regVal = PWM_0_GENB_ACTZERO_ONE;
		}
	}else{
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_1_GENA;
				regVal = PWM_1_GENA_ACTZERO_ONE;
		}
		else{
			regOffset = PWM1_BASE + PWM_O_1_GENB;
			regVal = PWM_1_GENB_ACTZERO_ONE;
		}
	}			
		
	// Program zero action to zero
	HWREG( regOffset ) = regVal;
}

/****************************************************************************
 Function
     PWM_UpDownNormal

 Parameters
     generatorNum 	(0 for generator0 or 1 for generator1 in PWM0)
		 channel 		(0 for channel A or 1 for channel B)

 Returns
     nothing

 Description
     Update pwm channel to normal up/down mode
 Notes

 Author
     John Talbot, 01/26/17
****************************************************************************/
void PWM_UpDownNormal( uint8_t generatorNum, uint8_t channel ){
	uint32_t regOffset;
	uint32_t regVal;
	
	// Determine appropriate generator
	if ( generatorNum == PWM0_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_0_GENA;
				regVal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO);
		}else{
			regOffset = PWM0_BASE + PWM_O_0_GENB;
			regVal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO);
		}
	}else if ( generatorNum == PWM0_GENERATOR_1 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_1_GENA;
				regVal = (PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO);
		}else{
			regOffset = PWM0_BASE + PWM_O_1_GENB;
			regVal = (PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO);
		}
	}else if ( generatorNum == PWM0_GENERATOR_3 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM0_BASE + PWM_O_3_GENA;
				regVal = (PWM_3_GENA_ACTCMPAU_ONE | PWM_3_GENA_ACTCMPAD_ZERO);
		}else{
			regOffset = PWM0_BASE + PWM_O_3_GENB;
			regVal = (PWM_3_GENB_ACTCMPBU_ONE | PWM_3_GENB_ACTCMPBD_ZERO);
		}
	}else if ( generatorNum == PWM1_GENERATOR_0 ){
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_0_GENA;
				regVal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO);
		}else{
			regOffset = PWM1_BASE + PWM_O_0_GENB;
			regVal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO);
		}
	}else{
		// Determine appropriate channel
		if ( channel == CHANNEL_A ){
				regOffset = PWM1_BASE + PWM_O_1_GENA;
				regVal = (PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO);
		}else{
			regOffset = PWM1_BASE + PWM_O_1_GENB;
			regVal = (PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO);
		}
	}
	
	// Check if PWM is set at 0 or always on
	if ( HWREG( regOffset ) != regVal ){
		// Restore normal functionality
		HWREG( regOffset ) = regVal;
	}
}

/*------------------------------ Test Harness -----------------------------*/
#ifdef TEST
// **** NOTE: This test harness relies on a periodic wide timer timeout 
// interrupt to function correctly. To ensure function, add PWMTestISR
// to the ISR vector table corresponding to wide timer 0A in 
// startup_rvmdk.S

#define TEST_UPDATE 2
#define NVIC_TMRA_EN BIT30HI
#define BOTH_GENS 2

#define LED_PORT GPIO_PORTF_BASE
#define RED_PIN GPIO_PIN_1
#define BLU_PIN GPIO_PIN_2
#define GRN_PIN GPIO_PIN_3

#define clrScrn() 	printf("\x1b[2J")

// define array of duty cycles to test
static float dutyCycleTest[3] = {20.f, 50.f, 80.f};

// ISR to capture timer input and set new duty cycle
void PWMTestISR( void ){
	// Initialize array counter
	static uint8_t counter = 0;
	static bool polarityNorm = false;
	
	// Clear interrupt
	HWREG( WTIMER0_BASE+TIMER_O_ICR ) = TIMER_ICR_TATOCINT;
	
	// Set duty cycle on all channels
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_0, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_0, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_1, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_1, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_3, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM0_GENERATOR_3, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM1_GENERATOR_0, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM1_GENERATOR_0, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM1_GENERATOR_1, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[counter], PWM1_GENERATOR_1, CHANNEL_B);
	
	// Update LED
	HWREG( LED_PORT+GPIO_O_DATA+ALL_BITS ) = (HWREG( LED_PORT+GPIO_O_DATA+ALL_BITS )
		& ~(RED_PIN | BLU_PIN | GRN_PIN)) | (BIT0HI << (counter+1));
	
	// Print status
	printf("Duty Cycle = %4.1f %%\r\n",dutyCycleTest[counter]);
	
	// Increment counter
	if ( counter == 0 ){		
		// Invert polarity
		if ( polarityNorm == true ){
			printf("\r\nINVERTED:\r\n\n");
			PWM_SetPolarity( PWM_INV_POL, PWM0_GENERATOR_0);
			//PWM_SetPolarity( PWM_INV_POL, PWM0_GENERATOR_1);
			polarityNorm = false;
		}else{
			printf("\r\nNORMAL:\r\n\n");
			PWM_SetPolarity( PWM_NORMAL_POL, PWM0_GENERATOR_0);
			//PWM_SetPolarity( PWM_NORMAL_POL, PWM0_GENERATOR_1);
			polarityNorm = true;
		}
		
		// Increment counter
		counter++;
	}else if ( counter == 2 ){
		// Reset counter
		counter = 0;
	}else{
		// Increment counter
		counter++;
	}
}

// Initialize PWM test timer interrupt
static void initPWMTestTimer( void ){
	// Enable clock to wide timer 1
	HWREG( SYSCTL_RCGCWTIMER ) |= SYSCTL_RCGCWTIMER_R0;
	
	// Loop until timer hardware is ready
	while( (HWREG( SYSCTL_PRWTIMER ) & SYSCTL_PRWTIMER_R0 ) != SYSCTL_PRWTIMER_R0 );
	
	// Disable timer A before configuring
	HWREG( WTIMER0_BASE+TIMER_O_CTL ) &= ~TIMER_CTL_TAEN;
	
	// Configure timer for 32bit (individual instead of concatenated)
	// Macro define 16bit refers to individual timer rather than actual 16bit
	HWREG( WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
	
	// Load 5s period into ILR register to fire interrupt every 5s
	HWREG( WTIMER0_BASE+TIMER_O_TAILR ) = (SYSTEM_CLOCK_FREQ * TEST_UPDATE);
	
	// Set Timer A into periodic mode
	HWREG( WTIMER0_BASE+TIMER_O_TAMR ) = (HWREG( WTIMER0_BASE+TIMER_O_TAMR ) &
		~TIMER_TAMR_TAAMS) | TIMER_TAMR_TAMR_PERIOD;
		
	// Set Timer A to count down
	HWREG( WTIMER0_BASE+TIMER_O_TAMR ) &= ~TIMER_TAMR_TACDIR;
		
	// Configure local capture interrupt in timer register
	HWREG( WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;
	
	// Enable interrupt in NVIC register
	HWREG( NVIC_EN2 ) |= NVIC_TMRA_EN;
	
	// Turn on interrupts globally
	__enable_irq();
	
	// Start timer and enable stall in debugging
	HWREG( WTIMER0_BASE+TIMER_O_CTL ) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

// Begin test harness to test effectiveness of functions in PWM_module
int main ( void ){
	// Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
	SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
			| SYSCTL_XTAL_16MHZ);
	TERMIO_Init();
	clrScrn();
	
	// Print test harness information
	puts("\rStarting Test Harness for PWM_Module\r\n");
	printf("%s %s\n",__TIME__, __DATE__);
	printf("\n\r\n");
	
	// Enable clock to port F
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5;
	
	// Loop until port is ready
	while( (HWREG( SYSCTL_PRGPIO ) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5 );
	
	// Enable LED's for output
	HWREG( LED_PORT+GPIO_O_DEN ) |= ( RED_PIN | BLU_PIN | GRN_PIN );
	HWREG( LED_PORT+GPIO_O_DIR ) |= ( RED_PIN | BLU_PIN | GRN_PIN );
	HWREG( LED_PORT+GPIO_O_DATA+ALL_BITS ) |= ( RED_PIN | BLU_PIN | GRN_PIN );	
	
	// Initialize 5 sec timer to go between
	initPWMTestTimer();
	
	// Initialize PWM hardware
	PWM_InitHardware();
	
	// Change PWM frequency to 5000Hz on gen0 and 2000 on gen1
	PWM_SetFrequency( 1000, PWM0_GENERATOR_0);
	PWM_SetFrequency( 1000, PWM0_GENERATOR_1);
	PWM_SetFrequency( 1000, PWM0_GENERATOR_3);
	PWM_SetFrequency( 1000, PWM1_GENERATOR_0);
	PWM_SetFrequency( 1000, PWM1_GENERATOR_1);
	
	// Set duty cycle on all channels
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_0, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_0, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_1, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_1, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_3, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[0], PWM0_GENERATOR_3, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[0], PWM1_GENERATOR_0, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[0], PWM1_GENERATOR_0, CHANNEL_B);
	
	PWM_SetDutyCycle( dutyCycleTest[0], PWM1_GENERATOR_1, CHANNEL_A);
	PWM_SetDutyCycle( dutyCycleTest[0], PWM1_GENERATOR_1, CHANNEL_B);
	
	// Loop forever
	while ( true );
}

// End of test harness
#endif

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

