/****************************************************************************
 Module
   BrushlessMotorTestSM.c

 Revision
   0.0.1

 Description
   This is a module for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 07/24/17 16:30 afb      starting modification for bl motor test
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "BrushlessMotorTestSM.h"
#include "PWM8Tiva.h"

/*----------------------------- Module Defines ----------------------------*/
#define ESC_BASE_DUTY 40    // esc signal: OFF at 40% duty, FULL ON at 80% duty
#define ESC_DUTY_OFF 40
#define CHANNEL_0 0         // PB6
#define THROTTLE_OFF 0
#define THROTTLE_INC 5      // increment throttle in this this step size
#define THROTTLE_FULL 100

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

uint8_t throttleToESCDuty(uint8_t throttle);
uint8_t clamp(uint8_t var, uint8_t upperLim, uint8_t lowerLim);
void printThrottleState(uint8_t throttle);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static BrushlessMotorState_t CurrentState;
static uint8_t throttle = 0;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitBrushlessMotorTestSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitBrushlessMotorTestSM ( uint8_t Priority )
{
  ES_Event ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitMotorTest;
  // init PWM module, using PB4
    PWM8_TIVA_Init();
  // set duty to ESC off value
    PWM8_TIVA_SetDuty(ESC_DUTY_OFF, CHANNEL_0);      //initializes motor to initial throttle
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
      return true;
  }else
  {
      return false;
  }
}

/****************************************************************************
 Function
     PostBrushlessMotorTestSM

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBrushlessMotorTestSM( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunBrushlessMotorTestSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event RunBrushlessMotorTestSM( ES_Event ThisEvent )
{
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch ( CurrentState )
  {
    case InitMotorTest :       // If current state is initial Psedudo State
        if ( ThisEvent.EventType == ES_INIT )// only respond to ES_Init
        {
           CurrentState = Off;
           throttle = THROTTLE_OFF;
           PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
           printThrottleState(throttle);
         }
         break;

    case Off :       // If current state is state one
      switch ( ThisEvent.EventType )
      {
        case ES_MORE : 
            CurrentState = On;  //more to On state
            throttle = THROTTLE_INC;
            PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
            printThrottleState(throttle);
            break;
        
        case ES_FULL : 
            CurrentState = On;//Decide what the next state will be
            throttle = THROTTLE_FULL;
            PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
            printThrottleState(throttle);
            break;
      }  // end switch on CurrentEvent
      break;
      
     case On :       // If current state is state one
      switch ( ThisEvent.EventType )
      {
        case ES_MORE : 
            if(throttle < THROTTLE_FULL ){        //only show an increase if throttle has headroom
                throttle = clamp( throttle + THROTTLE_INC, THROTTLE_FULL, THROTTLE_OFF);
                PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
                printThrottleState(throttle);
            }
            break;
        
        case ES_LESS : 
            if(throttle <= THROTTLE_INC){   //change to off state
                throttle = THROTTLE_OFF; 
                CurrentState = Off;
                printThrottleState(throttle);
            }
            else {      //otherwise just decrease
                throttle = clamp( throttle - THROTTLE_INC, THROTTLE_FULL, THROTTLE_OFF);
                PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
                printThrottleState(throttle);
            }
            break;
        
        case ES_FULL : 
            CurrentState = On;//Decide what the next state will be
            throttle = THROTTLE_FULL;
            PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
            printThrottleState(throttle);
            break;
        
        case ES_OFF : 
            CurrentState = Off; //shut motor off
            throttle = THROTTLE_OFF;
            PWM8_TIVA_SetDuty(throttleToESCDuty(throttle), CHANNEL_0);
            printThrottleState(throttle);
            break;
      }  // end switch on CurrentEvent
      break; 
 
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryBrushlessMotorTestSM

 Parameters
     None

 Returns
     BrushlessMotorState_t The current state of the Template state machine

 Description
     returns the current state of the Brushless Motor Test state machine
 Notes

 Author
    Drew Bell, 07/23/17, 19:21
****************************************************************************/
BrushlessMotorState_t QueryBrushlessMotorTestSM ( void )
{
   return(CurrentState);
}

/***************************************************************************
 private functions
 ***************************************************************************/

/****************************************************************************
 Function
     dutyToESCDuty

 Parameters
     uint8_t throttle value from 0 to 100

 Returns
     uint8_t ESC Duty value from 40 to 80

 Description
     converts a 0 to 100% throttle request value to a PWM duty that ESCs need
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/

uint8_t throttleToESCDuty(uint8_t throttle){
    uint8_t passedThrottle = throttle;
    // clamp duty to less than 100, unsigned int cannot be less than 0
    if(passedThrottle > 100){
        passedThrottle = 100;
    }
    
    //convert throttle to ESCDuty
    uint8_t ESCDuty = passedThrottle * 2 / 5 + ESC_BASE_DUTY;
    
    return ESCDuty;
}

//simple clamping function for uint8_t's

uint8_t clamp(uint8_t var, uint8_t upperLim, uint8_t lowerLim){
    uint8_t valToReturn = var;
    if(valToReturn > upperLim){
        valToReturn = upperLim;
    }
    else if (valToReturn < lowerLim){
        valToReturn = lowerLim;
    }
    return valToReturn;
}

void printThrottleState(uint8_t throttle){
    if(throttle == 0){
        printf("Motor OFF - Throttle = %d\n\r", throttle);
    }
    else if(throttle < 75 * THROTTLE_FULL / 100 ){
        printf("Motor ON  - Throttle = %d\n\r", throttle);
    }
    else if(throttle < THROTTLE_FULL){
        printf("Motor ON  - Throttle = %d (BE CAREFUL)\n\r", throttle);
    }
    else if(throttle == THROTTLE_FULL){
        printf("Motor ON  - Throttle = %d (DANGER ZONE)\n\r", throttle);
    }
    return;
}
   
uint8_t getInc(void){
    return THROTTLE_INC;
}
